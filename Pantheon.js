/*jshint esnext:true */
/**
 * @file
 * Works with pantheon.
 */

'use strict';

const helpers = require('./helpers.js');
const Site = require('./lib/Site.js');

/**
 * Represents an open session with Pantheon.
 */
class PantheonConnection {

  /**
   * Establish the Pantheon session.
   */
  constructor(machineToken) {
    let options = {};
    if (typeof machineToken !== 'undefined') {
      options['machine-token'] = machineToken;
    }
    this.connect = new Promise((resolve, reject) => {
      helpers.shellExec('terminus auth whoami --format=json', [], {}, true).then(
        resolve,
        (error) => {
          helpers.shellExec('terminus auth login', [], options).then(resolve, reject);
        });
    });
  }

  /**
   * Sends a command to Pantheon, after ensuring there's a connection.
   *
   * @param string command
   *   The command to run. eg. site create-env
   * @param array arguments
   *   (optional) Extra arguments to pass to the command. eg. 'add' or 'list'
   * @param object options
   *   (optional) Key-value pairs to pass as arguments to the command.
   *   eg. {site: 'my-site'}
   *
   * @return Promise
   *   A promise that resolves with the result of the command.
   */
  doCommand(command, args, options, json) {
    if (typeof options === 'undefined') options = {};
    if (typeof args === 'undefined') args = [];

    // Once connected, all commands should return json.
    options.format = 'json';

    // Automated calls need to always say yes.
    args.push('--yes');

    return this.connect.then(() => {
      return helpers.shellExec(`terminus ${command}`, args, options, true);
    });
  }

  /**
   * Get a list of all sites.
   *
   * @return Promise
   *   Resolves to an array of objects with site information.
   */
  getSites() {
    return new Promise((resolve, reject) => {
      this.doCommand('sites list').then((results) => {
        resolve(results.map(r => r.name));
      });
    });
  }

  /**
   * Get a specific site.
   *
   * @param string name
   *   The site's machine name.
   *
   * @return Site
   *   The requested site.
   */
  getSite(name) {
    return new Site(name, this);
  }
}

module.exports = PantheonConnection;
