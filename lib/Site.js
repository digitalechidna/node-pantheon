/*jshint esnext:true */
/**
 * @file
 * Represents a single site on pantheon.
 */

'use strict';

const Environment = require('./Environment.js');

class Site {

  /**
   * Store the data the site needs to operate.
   */
  constructor(siteName, PantheonConnection) {
    this.connection = PantheonConnection;
    this.name = siteName;
  }

  /**
   * Routes a command through the pantheon object, to the correct site.
   *
   * @param string command
   *   The command to run. eg. site create-env
   * @param array arguments
   *   (optional) Extra arguments to pass to the command. eg. 'add' or 'list'
   * @param object options
   *   (optional) Key-value pairs to pass as arguments to the command.
   *   eg. {site: 'my-site'}
   *
   * @return Promise
   *   A promise that resolves with the result of the command.
   */
  doCommand(command, args, options) {
    if (typeof options === 'undefined') options = {};

    // Set this site for the command.
    options.site = this.name;

    return this.connection.doCommand(`site ${command}`, args, options);
  }

  /**
   * Lists the environments currently set up on the site.
   *
   * @return Promise
   *  Resolves with an array of environment names.
   */
  getEnvironments() {
    return new Promise((resolve, reject) => {
      this.doCommand('environments').then((result) => {
        resolve(result.map(e => e.name));
      });
    });
  }

  /**
   * Get a specific environment.
   *
   * @param string name
   *   The environment name.
   *
   * @return Environment
   *   The requested environment.
   */
  getEnvironment(name) {
    return new Environment(name, this);
  }
}

module.exports = Site;
