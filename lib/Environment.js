/*jshint esnext:true */
/**
 * @file
 * Represents a single environment on pantheon.
 */

'use strict';

class Environment {

  /**
   * Store the data the environment needs to operate.
   */
  constructor(name, site) {
    // Validate the name. Pantheon does this anway, but it saves an API call.
    if (!/^[a-z][a-z0-9\-]{0,10}$/.test(name)) {
      throw new Error(`"${name}" is not a valid environment name.`);
    }
    this.site = site;
    this.name = name;
    this.doCommand = this.site.doCommand.bind(site);
  }

  /**
   * Checks if the environment exists.
   *
   * @param string name
   *   The name of the environment to check for.
   */
  check() {
    return new Promise((resolve, reject) => {
      let checkList = (list) => {
        resolve(list.indexOf(this.name) !== -1);
      };
      this.site.getEnvironments().then(checkList, reject);
    });
  }

  /**
   * Creates a new environment, from dev.
   *
   * @param string name
   *   The environment name.
   *
   * @return Promise
   *   Resolves when the environment completes successfully.
   */
  create() {
    // Tell pantheon to create the environment.
    let options = {
      'from-env': 'dev',
      'to-env': this.name,
    };
    return this.doCommand('create-env', [], options);
  }

  /**
   * Merges an environment to dev.
   *
   * @return Promise
   *   Resovles with the result of the merge.
   */
  merge() {
    return this.doCommand('merge-to-dev', [], {env: this.name});
  }

  /**
   * Deletes an environment.
   *
   * @param string name
   *   The environment name.
   *
   * @return Promise
   *   Resolves when the environment completes successfully.
   */
  remove() {
    return this.doCommand('delete-env', ['--remove-branch'], {env: this.name});
  }
}

module.exports = Environment;
