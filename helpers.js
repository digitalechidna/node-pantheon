/*jshint esnext:true */
/**
 * @file
 * Helper functions for the Pantheon module.
 */

'use strict';

const exec = require('child_process').exec;

module.exports = {

  /**
   * Helper for running shell commands.
   *
   * @param string command
   *   The command to run. eg. terminus site create-env
   * @param array arguments
   *   (optional) Extra arguments to pass to the command. eg. 'add' or 'list'
   * @param object options
   *   (optional) Key-value pairs to pass as arguments to the command.
   *   eg. {site: 'my-site'}
   * @param bool json
   *   (optional) True if the commands response should be parsed as json.
   *
   * @return Promise
   *   A promise that resolves with the result of the command.
   */
  shellExec(command, args, options, json) {

    // Set the default parameter values.
    if (typeof args === 'undefined') args = [];
    if (typeof options === 'undefined') options = {};
    if (typeof json === 'undefined') json = false;

    for (let i in options) {
      if (options.hasOwnProperty(i)) {
        args.push(`--${i}=${options[i]}`);
      }
    }

    command = `${command} ${args.join(' ')}`;

    return new Promise((resolve, reject) => {
      exec(command, (error, stdout, stderr) => {
        if (error !== null) {
          reject(error);
        }
        if (json) {
          try {
            resolve(stdout ? JSON.parse(stdout) : null);
          }
          catch (e) {
            reject(e.message);
          }
        }
        else {
          resolve(stdout);
        }
      });
    });
  }

};
